import copy
import sys
import time

from lxml import etree

from db_helper import get_fields


def measure_time(func):
    def time_it(*args, **kwargs):
        time_started = time.time()
        func(*args, **kwargs)
        time_elapsed = time.time()
        print("{execute} runtime is {sec} seconds for {element}"
              "".format(execute=func.__name__,
                        sec=time_elapsed - time_started,
                        element=args[0]))

    return time_it


# region CSV values generating and writing to file

csv_chunk_size = 10000
csv_buffer = []
unique_entities = set()


def generate_csv(values):
    values_list = []
    for v in values:
        if v is None:
            return
        fields = get_fields(v.get('entity'))
        if fields is None:
            print('No such table.')
            return
        row = tuple(map(lambda x: str(x), tuple(map(v.get, fields))))
        values_list.append((v.get('entity'), ''.join([str(row), '\n'])))
    return values_list


def chunk_csv(values=None):
    if values is not None:
        if len(csv_buffer) < csv_chunk_size:
            tuple(map(lambda x: csv_buffer.append(x), values))
            return

        write_csv()
        tuple(map(lambda x: csv_buffer.append(x), values))
        return

    write_csv()
    unique_entities.clear()


def write_csv():
    list(map(lambda x: unique_entities.add(x), [i[0] for i in csv_buffer]))
    for _ in unique_entities:
        ue = list(filter(lambda x: x[0] == _, csv_buffer))
        with open('csv/{}.txt'.format(_), 'a') as fp:
            list(map(lambda x: fp.write(x[1]), ue))
    csv_buffer.clear()


# endregion

# region Entities parsing


def get_children(element, lists=None):
    el_tree = {}
    children = list(element)

    if len(children) == 0:
        return None

    list_children = []
    for child in children:
        tag = str(child.tag).replace('-', '_')
        text = child.text

        if not str(text).isspace() and text is not None:
            if lists is not None and element.tag in lists:
                list_children.append({tag: text})
            else:
                el_tree.update({tag: text})
        if len(list(child)) != 0:
            if lists is not None and element.tag in lists:
                list_children.append(get_children(child, lists))
            else:
                ch = get_children(child, lists)
                if ch != {}:
                    el_tree.update({tag: ch})
    if len(list_children) != 0:
        el_tree.update({'_': list_children})
    return el_tree


def flatten_children(current, key, result):
    if isinstance(current, dict):
        for k in current:
            if current[k] is not None:
                new_key = "{0}__{1}".format(key, k) if len(key) > 0 else k
                flatten_children(current[k], new_key, result)
    else:
        result[key] = current
    return result


def get_region(_):
    values = get_children(_)
    values.update({'entity': _.tag})

    if _.find('federal_district') is not None:
        fd = _.find('federal_district').attrib['resource'].split('/')[-1]
        values.update({'federal_district': fd})

    stats = list(map(get_children, _.find('statistics')))
    for stat in stats:
        value = stat.get('value') if stat.get('value') is not None else ''
        if value == '':
            continue
        if stat.get('name') == 'Количество резюме':
            stat['code'] = 'cvs_count'
        if stat.get('code') is None \
                and stat.get('name') == 'Средняя заработная плата':
            stat['code'] = 'mean_salary'
        if stat.get('code') in \
                ['ALL', 'MICRO', 'SMALL', 'MIDDLE', 'BIG', 'LARGE']:
            values.update({stat.get('code').lower() + '_': value})
        else:
            values.update({stat.get('code').lower(): value})
    del values['statistics']

    flat_values = flatten_children(values, '', {})
    return [flat_values]


def get_industry(_):
    values = get_children(_)
    values.update({'entity': _.tag})

    flat_values = flatten_children(values, '', {})
    return [flat_values]


def get_prof(_):
    values = get_children(_)
    values.update({'entity': _.tag})

    values.update({'code': _.attrib['about'].split('#')[-1]})

    flat_values = flatten_children(values, '', {})
    return [flat_values]


def get_organization(_):
    values = get_children(_)
    values.update({'entity': _.tag})

    values.update({'code': _.attrib['about'].split('#')[-1]})
    values.update(
        {'region': _.find('region').attrib['resource'].split('#')[-1]})

    flat_values = flatten_children(values, '', {})
    return [flat_values]


def get_vacancy(_):
    values = get_children(_)
    values.update({'entity': _.tag})

    for tag in ['region', 'organization', 'industry', 'profession']:
        if _.find(tag) is None:
            values.update({tag: 'None'})
            continue
        values.update({tag: _.find(tag).attrib['resource'].split('#')[-1]})

    flat_values = flatten_children(values, '', {})
    return [flat_values]


def get_cv(_):
    lists = ['workExperienceList', 'educationList', 'additionalEducationList',
             # 'worldskills', 'competitions', 'qualification',
             # 'driveLicenceList', 'country', 'scheduleTypesList'
             ]

    values = get_children(_, lists)
    values.update({'entity': _.tag})

    values.update({'code': _.attrib['about'].split('#')[-1]})
    for tag in ['region', 'profession', 'industry']:
        if _.find(tag) is None:
            values.update({tag: 'None'})
            continue
        values.update({tag: _.find(tag).attrib['resource'].split('#')[-1]})

    lists_dicts = []
    for ls in lists:
        if values.get(ls) is not None:
            list_dict_values = values.get(ls).get('_')
            if list_dict_values is not None:
                for ldv in list(list_dict_values):
                    ldv.update({'entity': ls.split('List')[0]})
                    lists_dicts.append(ldv)
            del values[ls]
    # Пока не знаю, надо разобраться с возможными значениями
    for ls in ['worldskills', 'competitions', 'qualification']:
        values.update({ls: str(values.get(ls))})

    flat_values = flatten_children(values, '', {})
    for ls in ['driveLicenceList__driveLicences', 'country__country_name',
               'scheduleTypesList__scheduleType']:
        if flat_values.get(ls) is not None:
            flat_values.update({ls.split('__')[-1]: flat_values.get(ls)})
            del flat_values[ls]
    lists_dicts.append(flat_values)
    return lists_dicts


def get_response(_):
    return get_response_or_invitation(_)


def get_invitation(_):
    return get_response_or_invitation(_)


def get_response_or_invitation(_):
    values = get_children(_)
    values.update({'entity': _.tag})

    for tag in ['hiringOrganization', 'employment', 'activityFlag']:
        e = get_children(_.find(tag))
        if e is not None:
            values.update({tag: e if len(e) != 0 else None})

    flat_values = flatten_children(values, '', {})
    return [flat_values]


# endregion

def attr_match(attr, element):
    element_region = str(element.attrib.get('about')).split('#')[-1]
    if element_region == str(attr).split('#')[-1]:
        return element


# Объединение данных о регионах в одну xml
def merge_region_stats():
    r_stats = etree.parse("xml/{}.xml".format('region_stats')).getroot()
    r_cvs = etree.parse("xml/{}.xml".format('region_cv')).getroot()
    r_companies = etree.parse("xml/{}.xml".format('region_company')).getroot()

    regions = etree.Element('regions', attrib=r_stats.attrib)
    for r in list(r_stats):
        region = copy.deepcopy(r)

        attr = r.attrib.get('about')
        if attr is None:
            continue
        cv = tuple(filter(lambda x: attr_match(attr, x),
                          list(r_cvs)))[0]
        company = tuple(filter(lambda x: attr_match(attr, x),
                               list(r_companies)))[0]

        tuple(map(lambda x: region.find('statistics').append(x),
                  list(cv.find('statistics'))))
        tuple(map(lambda x: region.find('statistics').append(x),
                  list(company.find('statistics'))))

        regions.append(region)

    with open('xml/region.xml', 'w') as fp:
        fp.write(str(etree.tostring(regions, encoding='unicode')))

    pass


@measure_time
def parse(xml):
    open('csv/{}.txt'.format(xml), 'w').close()
    xml_file = "xml/full/{}.xml".format(xml)
    xsd_file = "xsd/{}.xsd".format(xml)
    # On validation: https://lxml.de/api/lxml.etree.iterparse-class.html
    schema = etree.XMLSchema(file=xsd_file)
    try:
        root = etree.iterparse(xml_file,
                               schema=schema,
                               events=('end',), tag=xml.split('_')[0])
        for event, element in root:
            values = entities.get(xml)(element)
            csv = generate_csv(values)
            chunk_csv(csv)
            element.clear()
            while element.getprevious() is not None:
                del element.getparent()[0]
        del root
        chunk_csv()
    except etree.XMLSyntaxError as Error:
        print(Error)


entities = {
    'region': get_region,
    'industry': get_industry,
    'prof': get_prof,
    'organization': get_organization,
    'vacancy': get_vacancy,
    'cv': get_cv,
    'response': get_response,
    'invitation': get_invitation,
}

if __name__ == '__main__':
    merge_region_stats()
    xml_list = sys.argv[1:]
    for _ in xml_list:
        parse(_)
