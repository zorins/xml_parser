import psycopg2

from psycopg2 import extras


def get_fields(table):
    query = create_table(table.split('_')[0])
    fields = tuple(map(lambda x: x.split(' ')[0],
                       query.split('FOREIGN')[0].split('(')[1].split(',')))

    return fields[:-1] if fields[-1] == '' else fields


tables = ['region',
          'industry',
          'prof',
          'organization',
          'vacancy',
          'cv',
          'workExperience',
          'education',
          'additionalEducation',
          'response',
          'invitation']


class DBHelper:
    def __init__(self):
        self.conn = psycopg2.connect(
            host="localhost",
            database="postgres",
            user="postgres",
            password="newPassword"
        )

    def clear_tables(self):
        c = self.conn.cursor()
        for _ in tables:
            c.execute("TRUNCATE TABLE {} CASCADE;".format(_))
        c.close()
        self.conn.commit()

    def drop_tables(self):
        c = self.conn.cursor()
        for _ in tables:
            c.execute("DROP TABLE IF EXISTS {} CASCADE".format(_))
        c.close()
        self.conn.commit()

    def setup(self):
        statements = list(map(create_table, tables))
        c = self.conn.cursor()
        for _ in statements:
            c.execute(_)
        c.close()
        self.conn.commit()

    def execute(self, query):
        c = self.conn.cursor()
        try:
            c.execute(query)
            c.close()
            self.conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

    def execute_batch(self, query, values):
        c = self.conn.cursor()
        try:
            extras.execute_batch(c, query, values)
            c.close()
            self.conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)


# region Create tables


def create_table(table_name):
    if table_name == 'region':
        return region_create_table()
    elif table_name == 'industry':
        return industry_create_table()
    elif table_name == 'prof':
        return prof_create_table()
    elif table_name == 'organization':
        return organization_create_table()
    elif table_name == 'vacancy':
        return vacancy_create_table()
    elif table_name == 'cv':
        return cv_create_table()
    elif table_name == 'workExperience':
        return work_experience_table()
    elif table_name == 'education':
        return education_table()
    elif table_name == 'additionalEducation':
        return additional_education_table()
    elif table_name == 'response':
        return response_create_table()
    elif table_name == 'invitation':
        return invitation_create_table()


def region_create_table():
    return str("CREATE TABLE IF NOT EXISTS region ("
               "code BIGINT NOT NULL PRIMARY KEY UNIQUE,"
               "name TEXT,"
               "federal_district TEXT,"
               'accomodation_accessibility TEXT,'
               'attraction_region TEXT,'
               'economic_growth TEXT,'
               'kindergarten_accessibility INT,'
               'medium_salary_difference TEXT,'
               'price_level TEXT,'
               'unemployment_level TEXT,'
               "all_ INT,"
               "micro_ INT,"
               "small_ INT,"
               "middle_ INT,"
               "big_ INT,"
               "large_ INT,"
               "cvs_count INT,"
               "mean_salary NUMERIC"
               ");")


def industry_create_table():
    return str("CREATE TABLE IF NOT EXISTS industry ("
               "code TEXT NOT NULL PRIMARY KEY UNIQUE,"
               "name TEXT,"
               "creationDate DATE,"
               "active BOOLEAN,"
               "dateModify DATE,"
               "deleted BOOLEAN"
               ");")


def prof_create_table():
    return str("CREATE TABLE IF NOT EXISTS prof ("
               "code INT NOT NULL PRIMARY KEY UNIQUE,"
               "name TEXT,"
               "creationDate DATE,"
               "category TEXT,"
               "etks TEXT,"
               "active BOOLEAN,"
               "dateModify DATE,"
               "deleted BOOLEAN"
               ");")


def organization_create_table():
    return str("CREATE TABLE IF NOT EXISTS organization ("
               "code TEXT NOT NULL PRIMARY KEY UNIQUE,"
               "region BIGINT,"
               "name TEXT,"
               "creationDate DATE,"
               "legalName TEXT,"
               "codeParentCompany TEXT,"  # TODO: Из этой же базы?
               "companyStructureHidden BOOLEAN,"
               "legalFormCode INT,"
               "legalFormName TEXT,"
               "description TEXT,"
               "ogrn BIGINT,"
               "inn BIGINT,"
               "kpp TEXT,"
               "addressCode BIGINT,"
               "address TEXT,"
               "site TEXT,"
               "firstRateCompany TEXT,"
               "hr_agency BOOLEAN,"
               "businessSize TEXT,"
               "source TEXT,"
               "stateProgram TEXT,"
               "innerInfo__codeExternalSystem TEXT,"
               "innerInfo__dateModify DATE,"
               "innerInfo__deleted BOOLEAN,"
               "innerInfo__externalId TEXT,"
               "innerInfo__idAuthor TEXT,"
               "innerInfo__idLogo TEXT,"
               "innerInfo__idSmallIcon TEXT,"
               "innerInfo__stateProgram TEXT,"
               "innerInfo__idUser TEXT,"
               "innerInfo__isModerated BOOLEAN,"
               "innerInfo__managerIds TEXT,"
               "innerInfo__moderationComment TEXT,"
               "innerInfo__moderationTime DATE,"
               "innerInfo__registrationStatus TEXT,"
               "innerInfo__status TEXT,"
               "innerInfo__changeTime DATE,"
               "innerInfo__disableImportInfo BOOLEAN,"
               "innerInfo__disableImportVacancy BOOLEAN,"
               "innerInfo__disableJoinCompany BOOLEAN,"
               "innerInfo__disableJoinManager BOOLEAN,"
               "FOREIGN KEY (region) REFERENCES region (code)"
               ");")


def vacancy_create_table():
    return str("CREATE TABLE IF NOT EXISTS vacancy ("
               "identifier TEXT NOT NULL PRIMARY KEY UNIQUE,"
               "region BIGINT,"
               "organization TEXT,"
               "industry TEXT,"
               "profession INT,"
               "creationDate DATE,"
               "datePosted TEXT,"
               "hiringOrganization__identifier TEXT,"
               "baseSalary__salary TEXT,"
               "baseSalary__salaryMin INT,"
               "baseSalary__salaryMax INT,"
               "title TEXT,"
               "employmentType TEXT,"
               "workHours TEXT,"
               "responsibilities TEXT,"
               "incentiveCompensation__accommodation__capability TEXT,"
               "incentiveCompensation__accommodation__housing TEXT,"
               "incentiveCompensation__transportCompensation TEXT,"
               "incentiveCompensation__retraining__capability TEXT,"
               "incentiveCompensation__retraining__condition TEXT,"
               "incentiveCompensation__retraining__grant TEXT,"
               "incentiveCompensation__retraining__grantValue TEXT,"
               "incentiveCompensation__premium__premiumType TEXT,"
               "incentiveCompensation__premium__premiumSize INT,"
               "incentiveCompensation__jobBenefits__benefits TEXT,"
               "incentiveCompensation__jobBenefits__otherBenefits TEXT,"
               "requirements__educationRequirements__educationType TEXT,"
               "requirements__educationRequirements__speciality TEXT,"
               "requirements__educationRequirements__academicDegree TEXT,"
               "requirements__experienceRequirements TEXT,"
               "requirements__priorityCategory TEXT,"
               "requirements__qualifications TEXT,"
               "requirements__requiredCertificates TEXT,"
               "jobLocation__address TEXT,"
               "jobLocation__additionalAddressInfo TEXT,"
               "jobLocation__geo__latitude NUMERIC,"
               "jobLocation__geo__longitude NUMERIC,"
               "socialProtecteds__socialProtected TEXT,"  # всегда одно значение
               "metroStations__metroStation TEXT,"  # всегда одно значение
               "oksoCode INT,"
               "needMedcard TEXT,"
               "requiredDriveLicence TEXT,"
               "source TEXT,"
               "workPlaces TEXT,"
               "additionalInfo TEXT,"
               "careerPerspective TEXT,"
               "innerInfo__metro_ids TEXT,"
               "innerInfo__sourceType TEXT,"
               "innerInfo__status TEXT,"
               "innerInfo__visibility TEXT,"
               "innerInfo__contactPerson TEXT,"
               "innerInfo__contactSource TEXT,"

               # TODO: несколько контактов?
               "innerInfo__contactList__contact__isModerated TEXT,"
               "innerInfo__contactList__contact__contactType TEXT,"
               "innerInfo__contactList__contact__idOwner TEXT,"
               "innerInfo__contactList__contact__isPreferred TEXT,"
               "innerInfo__contactList__contact__contactValue TEXT,"

               "innerInfo__dateModify DATE,"
               "innerInfo__deleted BOOLEAN,"
               "innerInfo__changeTime TEXT,"
               "vac_url TEXT,"
               "isUzbekistanRecruitment BOOLEAN"

               # ","
               # "FOREIGN KEY (region) REFERENCES region (code),"
               # "FOREIGN KEY (organization) REFERENCES organization (code),"
               # "FOREIGN KEY (industry) REFERENCES industry (code),"
               # "FOREIGN KEY (prof) REFERENCES prof (code)"
               ");")


def cv_create_table():
    return str("CREATE TABLE IF NOT EXISTS cv ("
               "code TEXT NOT NULL PRIMARY KEY UNIQUE,"
               "region BIGINT,"
               "profession INT,"
               "industry TEXT,"
               "positionName TEXT,"
               "creationDate DATE,"
               "locality BIGINT,"

               "educationType TEXT,"
               # "workExperienceList TEXT,"
               # "educationList TEXT,"
               # "additionalEducationList TEXT,"
               # Соревнования
               "worldskills TEXT,"
               # "worldskills__inspection_status TEXT,"
               # "worldskills__competence TEXT,"
               # "worldskills__skillAbbreviation TEXT,"
               # "worldskills__isInternational TEXT,"
               # "worldskills__internationalName TEXT,"
               # "worldskills__russianName TEXT,"
               "competitions TEXT,"
               # "competitions__abilympics_participation TEXT,"
               # "competitions__abilympics_inspection_status TEXT,"
               "qualification TEXT,"
               # "qualifications__nark__certificate TEXT,"
               # "qualifications__nark__participation TEXT,"
               # "qualifications__nark__inspection_status TEXT,"

               "driveLicences TEXT,"
               "country_name TEXT,"
               "publishDate TEXT,"
               "scheduleType TEXT,"
               "experience INT,"
               "priorityCategory TEXT,"
               "salary INT,"
               "skills TEXT,"
               "additionalSkills TEXT,"
               "busyType TEXT,"
               "relocation TEXT,"
               "businessTrips TEXT,"
               "candidateId TEXT,"
               "birthday INT,"
               "gender TEXT,"
               "addCertificates TEXT,"
               "retrainingCapability TEXT,"
               "otherInfo TEXT,"
               "innerInfo__idUser TEXT,"
               "innerInfo__status TEXT,"
               "innerInfo__visibility TEXT,"
               "innerInfo__dateModify DATE,"
               "innerInfo__deleted BOOLEAN,"
               "innerInfo__fullnessRate INT"

               # ","
               # "FOREIGN KEY (region) REFERENCES region (code),"
               # "FOREIGN KEY (industry) REFERENCES industry (code),"
               # "FOREIGN KEY (profession) REFERENCES prof (code)"
               ");")


def work_experience_table():
    return str("CREATE TABLE IF NOT EXISTS workExperience ("
               "idOwner TEXT,"
               "achievements TEXT,"
               "company_name TEXT,"
               "jobTitle TEXT,"
               "demands TEXT,"
               "dateFrom TEXT"
               ");")


def education_table():
    return str("CREATE TABLE IF NOT EXISTS education ("
               "idOwner TEXT,"
               "qualification TEXT,"
               "graduateYear INT,"
               "specialty TEXT,"
               "legalName TEXT,"
               "faculty TEXT"
               ");")


def additional_education_table():
    return str("CREATE TABLE IF NOT EXISTS additionalEducation ("
               "idOwner TEXT,"
               "courseName TEXT,"
               "graduateYear INT,"
               "legalName TEXT"
               ");")


def response_create_table():
    return str("CREATE TABLE IF NOT EXISTS response ("
               "identifier TEXT NOT NULL PRIMARY KEY UNIQUE,"
               "regionCode BIGINT,"
               "hiringOrganization__identifier TEXT,"
               "idCv TEXT,"
               "idVacancy TEXT,"
               "candidateId TEXT,"
               "idReply TEXT,"
               "creationDate DATE,"
               "dateModify DATE,"
               "deleted BOOLEAN,"
               "employment__candidateStatus TEXT,"
               "employment__date TEXT,"
               "employment__employerStatus TEXT,"
               "activityFlag__candidate TEXT,"
               "activityFlag__manager TEXT,"
               "isNew TEXT,"
               "recruitmentDate TEXT,"
               "responseType TEXT"

               # ","
               # "FOREIGN KEY (idCV) REFERENCES cv (code),"
               # "FOREIGN KEY (idVacancy) REFERENCES vacancy (identifier),"
               # 
               # "FOREIGN KEY (hiringOrganization__identifier) "
               # "REFERENCES organization (code),"
               # "FOREIGN KEY (regionCode) REFERENCES region (code)"
               ");")


def invitation_create_table():
    return str("CREATE TABLE IF NOT EXISTS invitation ("
               "identifier TEXT NOT NULL PRIMARY KEY UNIQUE,"
               "regionCode BIGINT,"
               "hiringOrganization__identifier TEXT,"
               "idCv TEXT,"
               "idVacancy TEXT,"
               "candidateId TEXT,"
               "idReply TEXT,"
               "creationDate DATE,"
               "dateModify DATE,"
               "deleted BOOLEAN,"
               "employment__candidateStatus TEXT,"
               "employment__date TEXT,"
               "employment__employerStatus TEXT,"
               "activityFlag__candidate TEXT,"
               "activityFlag__manager TEXT,"
               "isNew TEXT,"
               "recruitmentDate TEXT,"
               "responseType TEXT"

               # ","
               # "FOREIGN KEY (regionCode) REFERENCES region (code),"
               # "FOREIGN KEY (idVacancy) REFERENCES vacancy (identifier),"
               # 
               # "FOREIGN KEY (hiringOrganization__identifier) "
               # "REFERENCES organization (code),"
               # "FOREIGN KEY (idCV) REFERENCES cv (code)"
               ");")

# endregion
