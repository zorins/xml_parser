import sys
import time

from ast import literal_eval

from db_helper import DBHelper, get_fields


def measure_time(func):
    def time_it(*args, **kwargs):
        time_started = time.time()
        func(*args, **kwargs)
        time_elapsed = time.time()
        print("{execute} runtime is {sec} seconds for {element}\n"
              "".format(execute=func.__name__,
                        sec=time_elapsed - time_started,
                        element=args[0]))

    return time_it


@measure_time
def insert(_):
    fields = get_fields(_)
    fields_names = ','.join(fields)
    placeholders = ','.join(('%s',) * len(fields))
    sql_template = 'INSERT INTO {} ({}) VALUES ({}) ' \
                   'ON CONFLICT DO NOTHING' \
                   ';'.format(_.split('_')[0],
                              fields_names,
                              placeholders)

    with open('csv/{}.txt'.format(_), 'r') as fp:
        chunk = []
        chunk_count = 0
        chunk_size = 10000

        for value_string in fp:
            value_string = [x if x != 'None' else literal_eval(x)
                            for x in literal_eval(value_string)]
            if len(chunk) < chunk_size:
                chunk.append(value_string)
            else:
                DBHelper().execute_batch(sql_template, chunk)
                del chunk
                chunk = [value_string]
                chunk_count += 1
                print("This is {} chunk of size {}. Total rows: {}"
                      "".format(chunk_count, chunk_size,
                                chunk_count * chunk_size))
        # Остатки
        if len(chunk) != 0:
            DBHelper().execute_batch(sql_template, chunk)
        print("This is the last chunk of size {}. Total rows: {}"
              "".format(len(chunk),
                        chunk_count * chunk_size + len(chunk)))


if __name__ == '__main__':
    # DBHelper().clear_tables()
    DBHelper().setup()
    csv_list = sys.argv[1:]
    for _ in csv_list:
        insert(_)
